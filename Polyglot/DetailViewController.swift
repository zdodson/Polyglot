//
//  DetailViewController.swift
//  Polyglot
//
//  Created by Zackary Dodson on 6/11/17.
//  Copyright © 2017 Zackary Dodson. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var topLabel: UINavigationItem!
    @IBOutlet weak var scrollText: UITextView!
    
    func configureView() {
        // Update the user interface for the detail item.
        if detailItem != nil {
            if (scrollText) != nil {
                scrollText.text = detailItem
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: String? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    //MARK: Actions
    
    @IBAction func copyButton(_ sender: UIBarButtonItem) {
        UIPasteboard.general.string = scrollText.text
    }
}
