//
//  CodeLanguages.swift
//  Polyglot
//
//  Created by Zackary Dodson on 6/11/17.
//  Copyright © 2017 Zackary Dodson. All rights reserved.
//

import UIKit

class CodeLanguage {
    var name: String
    var KindList = [String]()
    var DataList = [String]()
    
    init?(name: String) {
        
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        // Initialize stored properties.
        self.name = name
        let filemanager:FileManager = FileManager()
        let docPath = Bundle.main.resourcePath! + "/" + self.name
        let files = filemanager.enumerator(atPath: docPath)
        
        if (files != nil) {
            while let file = files!.nextObject() as? String {
                let kind = file.replacingOccurrences(of: "_", with: " ", options: .literal, range: nil)
                self.KindList.append(kind)
                let path: String = Bundle.main.path(forResource: file, ofType: "", inDirectory: self.name)!
                let data = try? String(contentsOfFile: path, encoding: String.Encoding.utf8)
                self.DataList.append(data!)
            }
        }
    }
    
    func setData(kind: String, file: String) {
        self.KindList.append(kind)
        let path: String = Bundle.main.path(forResource: file, ofType: "", inDirectory: self.name)!
        let data = try? String(contentsOfFile: path, encoding: String.Encoding.utf8)
        self.DataList.append(data!)
    }
    
    func getKindList() -> [String] {
        return self.KindList
    }
    
    func getData(kind: String) -> String {
        for (i,type) in KindList.enumerated() {
            if type == kind {
                return DataList[i]
            }
        }
        return ""
    }
    
    func getKind(item: Int) -> String {
        return self.KindList[item]
    }
}
