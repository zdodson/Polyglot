//
//  CategoryViewController.swift
//  Polyglot
//
//  Created by Zackary Dodson on 6/12/17.
//  Copyright © 2017 Zackary Dodson. All rights reserved.
//

import UIKit

class CategoryViewController: UITableViewController {
    
//    @IBOutlet weak var tableView: UITableView!
    var thisCategoryList = [String]()
    var filteredCategoryList = [String]()
    let searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchVC()
        configureView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupSearchVC() {
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.searchBar.sizeToFit()
        tableView.tableHeaderView = searchController.searchBar
    }
    
    // MARK: - Table View
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredCategoryList = thisCategoryList.filter { Category in
            return Category.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return filteredCategoryList.count
        }
        return thisCategoryList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Category", for: indexPath) as UITableViewCell
        cell.textLabel!.text = getCorrectCategory(row: indexPath.row)
        return cell
    }
    
    var thisCodeLanguage: CodeLanguage? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        thisCategoryList = (thisCodeLanguage?.getKindList)!()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Detail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let detailViewController = segue.destination as! DetailViewController
                let selectedText = getCorrectCategory(row: indexPath.row)
                detailViewController.detailItem = thisCodeLanguage?.getData(kind: selectedText)
                detailViewController.topLabel.title = (thisCodeLanguage?.name)! + " - " + (thisCodeLanguage?.getKind(item: indexPath.row))!
            }
        }
    }
    
    func getCorrectCategory(row: Int) -> String {
        var text: String
        if searchController.isActive {
            text = filteredCategoryList[row]
        } else {
            text = thisCategoryList[row]
        }
        return text
    }
    
    deinit {
        self.searchController.view.removeFromSuperview()
    }
}

extension CategoryViewController: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
}
