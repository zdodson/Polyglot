//
//  MasterViewController.swift
//  Polyglot
//
//  Created by Zackary Dodson on 6/11/17.
//  Copyright © 2017 Zackary Dodson. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var CodeLanguages = [CodeLanguage]()
    var filteredCodeLanguages = [CodeLanguage]()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchVC()
        loadCodes()
    }
    
    private func loadCodes() {
        CodeLanguages.append(CodeLanguage(name: "C++")!)
        CodeLanguages.append(CodeLanguage(name: "Fortran")!)
        CodeLanguages.append(CodeLanguage(name: "MATLAB")!)
        CodeLanguages.append(CodeLanguage(name: "Python")!)
    }
    
    func setupSearchVC() {
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.searchBar.sizeToFit()
        tableView.tableHeaderView = searchController.searchBar
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Categories" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let categoryViewController = segue.destination as! CategoryViewController
                categoryViewController.thisCodeLanguage = getCorrectCodeLanguage(row: indexPath.row)
            }
        }
    }

    // MARK: - Table View
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredCodeLanguages = CodeLanguages.filter { CodeLanguage in
            return CodeLanguage.name.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return filteredCodeLanguages.count
        }
        return CodeLanguages.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CodeLanguage", for: indexPath)
        let object = getCorrectCodeLanguage(row: indexPath.row)
        cell.textLabel!.text = object.name
        return cell
    }
    
    func getCorrectCodeLanguage(row: Int) -> CodeLanguage {
        let object: CodeLanguage
        if searchController.isActive {
            object = filteredCodeLanguages[row]
        } else {
            object = CodeLanguages[row]
        }
        return object
    }
}

extension MasterViewController: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
}

extension UINavigationController {
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .all
    }
}
